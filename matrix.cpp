//
// Created by Luke on 9/26/2017.
//
#include "matrix.h"
matrix::matrix(int r, int c){       //initializes to 0
    row = r;
    column = c;
    mat = new int*[row];
    for (int i = 0; i < row; i++){
        mat[i] = new int[column];
    }
    for (int i = 0; i<row; i++){
        for (int j = 0; j < column; j++) {
            mat[i][j] = 0;

        }

    }
}

matrix::matrix (int r, int c, char one){//initialize to 1
    row = r;
    column = c;
    mat = new int*[row];
    for (int i = 0; i < row; i++){
        mat[i] = new int[column];
    }

    for (int i = 0; i<row; i++){
        for (int j = 0; j < column; j++) {
            mat[i][j] = 1;
        }

    }
}

matrix matrix::operator+(const matrix& obj){
    matrix temp(4,5);
    //int length = this->row;
    for(int i = 0; i < obj.row; i++){
        for(int j = 0; j <obj.column; j++){
            temp.mat[i][j] = this->mat[i][j]+obj.mat[i][j];
        }
    }
    return temp;
}

matrix matrix::operator-(const matrix& obj) {
    matrix temp (4,5);
    for(int i = 0; i < obj.row; i++){
        for(int j = 0; j <obj.column; j++){
            temp.mat[i][j]=this->mat[i][j]-obj.mat[i][j];
        }
    }
    return temp;
}

matrix matrix::operator*(const matrix& obj) {
    matrix temp (4, 5);
    for(int i = 0; i < row; i++){
        for(int j = 0; j <column; j++){
            temp.mat[i][j]= this->mat[i][j]*obj.mat[i][j];
        }
    }

    return temp;


}

std::ostream& operator << (std::ostream& os, const matrix& obj){


    for (int i = 0; i < obj.row; i++){
        for (int j = 0; j < obj.column; j++){
           os << obj.mat[i][j] << " ";
        }
        std::cout << std::endl;



    }
    return os;


}
matrix& matrix::operator =(const matrix &obj) {
    //std::cout << "Overload = called" << std::endl;
    if (this == &obj) {     //checks if same
        return *this;
    }
    else{
    if (mat != NULL)
        for (int i = 0; i < obj.row; i++) {     //destroys 2d array
            delete[] mat[i];
        }
        delete[] mat;
    }


    row = obj.row;
    column = obj.column;
   mat = new int*[row];     //make new 2d array
    for (int i = 0; i < row; i++){
        mat[i] = new int[column];
    }



    for (int i = 0; i < row; i++){
        for(int j = 0; j < column; j++){        //set new array to the object (new = obj)
                        mat[i][j] = obj.mat[i][j];
        }
    }




    return *this;
}

matrix::~matrix(){
        for (int i = 0; i < 4; i++) {
            delete[] mat[i];
        }
        delete[] mat;
    }


matrix::matrix (int r, int c,std::string random){
    row = r;
    column = c;
    mat = new int*[row];
    for (int i = 0; i < row; i++){
        mat[i] = new int[column];
    }
    for (int i = 0; i<row; i++){
        for (int j = 0; j < column; j++) {
            mat[i][j] = rand();

        }

    }
}