//
// Created by Luke on 9/26/2017.
//

#ifndef LAB_EXAM_1_MATRIX_H
#define LAB_EXAM_1_MATRIX_H

#include <iostream>
#include <string>
#include <random>

class matrix{
private:
    int **mat;      //2d array
    int row;
    int column;
public:
    matrix operator+(const matrix&);
    matrix operator-(const matrix&);
    matrix operator*(const matrix&);
    friend std::ostream& operator<< (std::ostream&, const matrix&);


    matrix (int r, int c);  //sets to zero
   matrix (int r, int c, std::string random);   //sets random number
    matrix (int r, int c, char one);    //sets to 1
    matrix&operator =(const matrix&);   //having issues with this, I think it works
    virtual ~matrix();
};

#endif //LAB_EXAM_1_MATRIX_H
